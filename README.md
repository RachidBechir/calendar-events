# To start app locally 

If you already have node and npm installed locally
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

---

If you have docker installed locally
### `docker run -p 3000:3000`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

---

If have docker-compose installed locally

### `docker-compose up -d --build`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
