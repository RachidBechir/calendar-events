import { useState, useEffect } from 'react';
import moment from 'moment';

function App() {

  const inputData = [
    {
      "id": 1,
      "start": "17:00",
      "duration": 60
    },
    {
      "id": 2,
      "start": "17:00",
      "duration": 120
    },
    {
      "id": 3,
      "start": "19:40",
      "duration": 10
    },
    {
      "id": 4,
      "start": "15:00",
      "duration": 20
    },
    {
      "id": 5,
      "start": "18:00",
      "duration": 60
    },
    {
      "id": 6,
      "start": "10:25",
      "duration": 35
    },
    {
      "id": 7,
      "start": "10:45",
      "duration": 30
    },
    {
      "id": 8,
      "start": "17:00",
      "duration": 60
    },
    {
      "id": 9,
      "start": "10:00",
      "duration": 30
    },
    {
      "id": 10,
      "start": "11:50",
      "duration": 20
    },
    {
      "id": 11,
      "start": "19:00",
      "duration": 60
    },
    {
      "id": 12,
      "start": "09:00",
      "duration": 45
    },
    {
      "id": 13,
      "start": "14:45",
      "duration": 60
    },
    {
      "id": 14,
      "start": "19:20",
      "duration": 10
    },
    {
      "id": 15,
      "start": "11:50",
      "duration": 30
    },
    {
      "id": 16,
      "start": "11:40",
      "duration": 40
    },
    {
      "id": 17,
      "start": "14:00",
      "duration": 30
    }    
  ];

    // Number of hours that will be show in the window screen
  const HOUR = 12;

    // Number of minutes that will be show in the window screen
  const MIN_IN_HOURE = HOUR*60; // 12*60
  const [height, setHeight] = useState(window.innerHeight);

    // Represent hour by pixels (1 hour = x px)
    // x = height of window / number of hours that will be show in the window screen
  const [hour, setHour] = useState(height / HOUR)

    //  Represent minute by pixels (1 minute = x px)
    //  x = height of window / number of minutes that will be show in the window screen
  const [minute, setMinute] = useState(height / MIN_IN_HOURE);
  
  useEffect(() => {
      // Respond to window resize events
    const handleResize = ()=> {
      setHeight(window.innerHeight); 
      setHour(height/HOUR);
      setMinute(height/MIN_IN_HOURE);
    }

    window.addEventListener('resize', handleResize);
  });

    // Sort inputData by start time
  const sortInputData = (data) => {
    data.sort((a, b) => {
      if (a.start > b.start) return 1;
      if (a.start < b.start) return -1;
      return 0;
    });
  }

  // getReferanceElement return an element where we can referance about it to place element 
  // before it if ther is place or we shift element after it 
  const getReferanceElement = (data, currentStart, currentEnd) => {
    return data.filter(element => {
      const elementEnd = moment(element.start, 'h:mm').add(element.duration, 'minutes').format("HH:mm");
      return currentStart < elementEnd && currentEnd < elementEnd 
    })
    .sort((a, b) => {
    if (a.duration > b.duration) return 1;
    if (a.duration < b.duration) return -1;
    return 0;
    })[0];
  }

  // getTopElement return an element where we can place other element directly under it 
  const getTopElement = (data, currentStart) => {
    return data.find(element => {
      const elementEnd = moment(element.start, 'h:mm').add(element.duration, 'minutes').format("HH:mm");
      return elementEnd === currentStart;
    });
  }

    // "overrideInputData" will add "left" and "width" attribute to each event (inputData element) 
  // "left" and "width" will be used to display events in window screen
  const overrideInputData = (data) => {
    sortInputData(data);
    let topElementLeft = 0;
    let startOfTab = 0;
    let start = data[0].start;
    let end = moment(start, 'h:mm').add(data[0].duration, 'minutes').format("HH:mm");
    data[0].left = 0;
    data[0].width = '100%';
    
    for (let i = 1; i < data.length; i++) {
      
      const element = data[i];
      const currentStart = element.start;
      const currentEnd = moment(currentStart, 'h:mm').add(element.duration, 'minutes').format("HH:mm");
        // "newTab" is a table where we search for the "referanceElement" and "topElement"
      const newTab = data.slice(startOfTab, i);
      const referanceElement = getReferanceElement(newTab, currentStart, currentEnd);
      const topElement = getTopElement(newTab, currentStart);
      
      if (currentStart >= start && currentStart <= end) {
        if (topElement) {
            // if ther is a "topElement" we place the current element under it directly then
            // we increment the startOfTab
          element.left = topElementLeft;
          topElementLeft = topElementLeft + topElement.left + 25;
          startOfTab = data.indexOf(topElement) + 1;
          element.width = '20px';
        } else if (referanceElement) {
            // if ther is a "referanceElement" we shift the current element after it then
            // we increment the startOfTab
          startOfTab++;
          referanceElement.width = '20px';
          element.width = '100%';
          element.left = referanceElement.left + 25;
        } else {
          startOfTab++;
          data[i - 1].width = '20px';
          element.width = '100%';
          element.left = data[i - 1].left + 25;
        }
        
      } else if (currentStart > end && referanceElement) {
        element.left = referanceElement.left + 25;
        element.width = '100%';
      } else {
        element.left = 0;
        element.width = '100%';
      }

      start = currentStart;
      end = currentEnd;

    }

    return data;
  }

    const overriddenInputData = overrideInputData(inputData);

  return (
    <div>
      {
        overriddenInputData.map((item) => {
          const hours = moment(item.start, 'h:mm').get('hour');
          const minutes = moment(item.start, 'h:mm').get('minutes');
          const styles = {
            color: 'white',
            backgroundColor: 'blue',
            border: '1px solid black',
            position: 'fixed',
             // to make "09:00" be the top of the window and "21:00" be the bottom
            top: `${(hours - 9) * hour + minutes * minute}px`,
            height: `${minute * item.duration}px`,
            left:`${item.left}px`,
            width: `${item.width}`,
          };
          return (
            <div style={styles} key={item.id} >
              {item.id}
            </div>
          )
        })
      }
    </div>
  );
}

export default App;
